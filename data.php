<?php 
	require_once('helper.php');
	
	if (empty($_GET)) {

		$query 	= "SELECT * FROM note ORDER BY id DESC";
		$sql	= mysqli_query($db_connect,  $query);

		if ($sql) {
			$result = array();
			while($row = mysqli_fetch_array($sql)){
				array_push($result,array(
					'id'	=> $row['id'],
					'note'	=> $row['content']
				));
			}
			
			echo json_encode( array( 'notes' => $result ));	
		} else {
			echo json_encode( array("message" => "error") );
		}

	} else {

		$id = $_GET['id'];

		$query 	= "SELECT * FROM note WHERE id='$id' ";
		$sql	= mysqli_query($db_connect,  $query);

		if ($sql) {
			$result = array();
			while($row = mysqli_fetch_array($sql)){
				$result = array(
					'id'	=> $row['id'],
					'note'	=> $row['content']
				);
			}
			
			echo json_encode( array( 'note' => $result ));	
		} else {
			echo json_encode( array("message" => "error") );
		}

	}
	
	mysqli_close($db_connect);

 ?>